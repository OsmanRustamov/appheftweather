module com.osman.appneftpogoda {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.bootstrapfx.core;
    requires org.jsoup;

    opens com.osman.appneftpogoda to javafx.fxml;
    exports com.osman.appneftpogoda;
}