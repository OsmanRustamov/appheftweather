package com.osman.appneftpogoda.userinterface;

import com.osman.appneftpogoda.parser.Parser;

import java.io.IOException;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.shape.*;
import javafx.stage.Stage;
public class UserInterface extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Circle c = new Circle(100);
        Pane p = new Pane();
        p.getChildren().add(c);

        Scene s = new Scene(p, 500, 500);
        stage.setScene(s);
        stage.show();
    }
}
