package com.osman.appneftpogoda;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class ScrollableRectangles extends Application {

    private static final int SCENE_WIDTH = 400;
    private static final int SCENE_HEIGHT = 400;
    private static final int RECTANGLE_WIDTH = 100;
    private static final int RECTANGLE_HEIGHT = 50;
    private static final int NUM_RECTANGLES = 7;

    @Override
    public void start(Stage primaryStage) {
        Pane pane = new Pane();
        ScrollPane scrollPane = new ScrollPane(pane);

        for (int i = 0; i < NUM_RECTANGLES; i++) {
            Rectangle rectangle = createRectangle(i);
            pane.getChildren().add(rectangle);
        }

        Scene scene = new Scene(scrollPane, SCENE_WIDTH, SCENE_HEIGHT);
        primaryStage.setScene(scene);
        primaryStage.show();

        animateRectangles(pane);
    }

    private Rectangle createRectangle(int index) {
        int y = index * RECTANGLE_HEIGHT;
        Rectangle rectangle = new Rectangle(RECTANGLE_WIDTH, RECTANGLE_HEIGHT, Color.BLUE);
        rectangle.setLayoutY(y);
        return rectangle;
    }

    private void animateRectangles(Pane pane) {
        pane.getChildren().forEach(node -> {
            if (node instanceof Rectangle) {
                Rectangle rectangle = (Rectangle) node;
                animateRectangle(rectangle);
            }
        });
    }

    private void animateRectangle(Rectangle rectangle) {
        double endY = SCENE_HEIGHT - RECTANGLE_HEIGHT;
        double duration = (endY - rectangle.getY()) / RECTANGLE_HEIGHT * 1000;

        javafx.animation.TranslateTransition transition = new javafx.animation.TranslateTransition(
                javafx.util.Duration.millis(duration), rectangle);
        transition.setToY(endY);
        transition.setOnFinished(e -> {
            rectangle.setY(0);
            animateRectangle(rectangle);
        });
        transition.play();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
