package com.osman.appneftpogoda.parser;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.HashMap;
import java.util.ArrayList;
public class Parser {
    private static Document getPage() throws IOException {
        return Jsoup.connect("https://yandex.ru/pogoda/nephtekamsk").get();
    }
    public HashMap getWeather() throws IOException {
        HashMap <String, ArrayList> weather = new HashMap<>();
        ArrayList <String> day = new ArrayList<>();
        ArrayList <String> date = new ArrayList<>();
        ArrayList <String> temp = new ArrayList<>();
        ArrayList <String> conditions = new ArrayList<>();
        Document page = getPage();
        Elements tableWth = page.select("body > div > div > div.forecast-briefly > div.forecast-briefly__days  > ul > li");
        for (Element wth : tableWth) {
            day.add(wth.select("div > div.forecast-briefly__name").text());
            date.add(wth.select("div > time").text());
            temp.add(wth.select("div > div.temp > span").text());
            conditions.add(wth.select("div > div.forecast-briefly__condition").text());
        }
        weather.put("day", day);
        weather.put("date", date);
        weather.put("temp", temp);
        weather.put("conditions", conditions);
        return weather;
    }
}