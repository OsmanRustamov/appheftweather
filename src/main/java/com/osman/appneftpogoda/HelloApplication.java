package com.osman.appneftpogoda;

import com.osman.appneftpogoda.parser.Parser;
import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class HelloApplication extends Application
{
    @Override
    public void start(Stage stage) throws IOException
    {
        Parser parser = new Parser();
        HashMap wth = parser.getWeather();
        ArrayList<String> days = (ArrayList<String>) wth.get("day");
        ArrayList<String> dates = (ArrayList<String>) wth.get("date");
        ArrayList<String> temps = (ArrayList<String>) wth.get("temp");
        ArrayList<String> conditions = (ArrayList<String>) wth.get("conditions");
        Pane p = new Pane();
        int y = 0;
        int l = 20;
        for (int i = 0; i < 7; i++) {
            Rectangle rect = new Rectangle(190, 150, Color.WHITE);
            rect.setStroke(Color.BLACK);
            rect.setX(10);
            rect.setY(10 + y);
            p.getChildren().add(rect);

            Text day = new Text(days.get(i));
            day.setX(20);
            day.setY(30 + y);
            p.getChildren().add(day);

            Text date = new Text(dates.get(i));
            date.setX(20);
            date.setY(60 + y);
            p.getChildren().add(date);

            Text temp = new Text(temps.get(i));
            temp.setX(20);
            temp.setY(90 + y);
            p.getChildren().add(temp);

            Text condition = new Text(conditions.get(i));
            condition.setX(20);
            condition.setY(120 + y);
            p.getChildren().add(condition);

            y += 160;
            ScrollBar vertScroll = new ScrollBar();
            vertScroll.setOrientation(Orientation.VERTICAL);
            vertScroll.setLayoutX(195);
            vertScroll.setMin(0);
            vertScroll.setMax(2303);
        }

//        vertScroll.setOrientation(Orientation.VERTICAL);
//        vertScroll.setMax(1210);
//        vertScroll.setMin(0);
//        p.getChildren().add(vertScroll);
//        ScrollPane vertScroll = new ScrollPane();

        Scene scene = new Scene(p, 210, 329);
        stage.setTitle("Weather in Neftekamsk");
        stage.setScene(scene);
        stage.show();
    }

}